const open = require('amqplib').connect(process.env.AMQP_URL);

var computeQueue = 'csvcompute';
var producerQueue = 'csvproducer';

class ComputeClusterMaster {

  constructor() {
    this.openChannel();
    this.onResult = function() {};
  }

  openChannel() {
    var _this = this;
    open.then(function(conn) {
      console.log('RMQ_MASTER: channel created.');
      return conn.createChannel();
    }).then(function(ch) {
      ch.assertQueue(producerQueue).then(function(ok) {
        ch.consume(producerQueue, function(msg) {
          console.log(`received id: ${msg.properties.correlationId}`);
          _this.onResult(msg.content.toString());
          ch.ack(msg);
          return;
      })
      });
      return;
    }).catch(console.warn);
  }
  startNewCalculatorWork(csv, timestamp, source) {
    console.log(csv.length, timestamp);
    open.then(function(conn) {
      return conn.createChannel();
    }).then(function(ch) {
      console.log('RMQ_MASTER: sending to queue.');
      return ch.assertQueue(computeQueue).then(function(ok) {
        ch.sendToQueue(computeQueue, Buffer.from(csv), {
          replyTo: producerQueue,
          correlationId: timestamp,
          headers: {'source': source }
        });
        return;
      });
      return;
    }).catch(console.warn);
  }
}

module.exports = ComputeClusterMaster;