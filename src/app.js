const Router = require('router');
const http = require('http');
const RabbitMaster = require('./rabbit_master');
const multer = require('multer');
const fs = require('fs');
const { exec } = require('child_process');
const DiffRenderer = require('./htmlRenderer'); 

var router = Router()
var rmaster = new RabbitMaster();

// router.use(bodyParser.json())
var upload = multer({ dest: 'uploads/' });

router.post('/verify', upload.fields([{ name: 'input', maxCount: 1},  {name: 'output', maxCount: 1  }]), function (req, res) {
    if (req.files) {
        console.log(req.files['input'][0].path);
        console.log(req.files['output'][0].path);
        let inputCsv = fs.readFileSync('./'+req.files['input'][0].path,'utf8').toString();
        var source = req.body.source != null ? req.body.source : 'Firefly';
        rmaster.onResult = function(content) {
            var fileName = 'diff.csv';
            var filePath = `./${fileName}`;
            var html = 'output.html';
            fs.writeFileSync(filePath,content,'utf8');

            let compareCsv = fs.readFileSync('./'+req.files['output'][0].path,'utf8').toString();
            var renderer = new DiffRenderer(content, compareCsv, req.body);
            fs.writeFileSync(`./${html}`,renderer.toString(),'utf8');

            res.writeHead(200, {
                "Content-Type": "text/html",
                "Content-Disposition": "attachment; filename=" + html,
                });
            fs.createReadStream(html).pipe(res);
        };
        rmaster.startNewCalculatorWork(inputCsv, Date.now().toString(), source);
    }
})
 
var server = http.createServer(function(req, res) {
    router(req, res, function(req, res) {

    });
});
 
server.listen(7770);

console.log('Server running at http://localhost:7770/');