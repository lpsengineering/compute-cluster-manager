const csvParse = require('csv-parse/lib/sync');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const percentDiff = require('percentage-difference');
const moment = require('moment');

class CSVDiffHTMLRenderer {

    constructor(first, second, options) {
        this.conflictsOnly = false;
        this.threshold = 0.5;
        this.source = 'Firefly';
        console.log(options);
        if (options != null) {
            if (options.conflictsOnly != null) {
                this.conflictsOnly = options.conflictsOnly.toString() == 'true';
            }
            if (options.threshold != null){
                this.threshold = options.threshold;
            }
            if (options.source != null) {
                this.source = options.source;
            }
        }
        this.firstObj = this.decodeCSVToPojo(first);
        this.secondObj = this.decodeCSVToPojo(second);

        //TODO: convert from csv string to objects
        this.generateHTMLResult();
    }

    decodeCSVToPojo(input) {
        const records = csvParse(input, {
            columns: true,
            skip_empty_lines: true
        });
        return records;
    }

    getTemplateHead() {
        return fs.readFileSync(path.resolve(__dirname + '/template_head.html'),'utf8').toString();
    }

    generateHTMLResult() {
        this.result = this.getTemplateHead();
        console.log(`generating ${this.source} HTML`);
        if (this.source == 'Firefly') {
            var fireflyBody = this.generateFireflyBody();
            this.result += fireflyBody.summary;
            this.result += fireflyBody.body;
        } else if (this.source == 'Ace AU') {
            var aceBody = this.generateAceAUBody();
            this.result += aceBody.summary;
            this.result += aceBody.body;
        } else if (this.source == 'Hertz') {
            var hertzBody = this.generateHertzBody();
            this.result += hertzBody.summary;
            this.result += hertzBody.body;
        }
        this.result += '</table></div></body></html>';
    }

    generateHertzBody() {
        var body = '';
        var summary = '';
        //generate headers
        body += `
        <table><tr>
        <th>Key</th>
        <th>Car Code</th>
        <th>Product Code</th>
        <th>Duration</th>
        <th>Startdate</th>
        <th>Comparison Output (MarginFuel)</th>
        <th>Calculated Output</th>
        <th>Difference %</th>
        <th>Calculation</th>
        </tr>`;

        var ctr = -1;
        var matchingRows = 0;
        var totalRows = this.firstObj.length;

        const reducer = (acc, cur) => {
            var startingDate = cur.startDate || cur.Startdate;
            let formattedDate = moment(startingDate, [
                "MM-DD-YYYY",
                "MM-DD-YY",
                "MM-DD-YYYY HH:mm",
                "MM-DD-YY HH:mm"
            ]).format("DD/MM/YYYY HH:mm");
            if(formattedDate === 'Invalid date') {
                // TODO how to handle if date is invalid
                console.log(startingDate, 'is an invalid date');
            } else {
                startingDate = formattedDate;
            }
            cur.ProductCode = cur.ProductCode || cur['Product Code'];
            cur.sortKey = cur.ProductCode + '-' + cur.carCode + '-' + startingDate;
            acc[cur.sortKey] = cur;
            return acc;
        };

        // Convert data to map, use sortKey as key
        console.log('Reducing FIRST');
        let first = this.firstObj.reduce(reducer, {});
        console.log('Reducing SECOND');
        let second = this.secondObj.reduce(reducer, {});


        let merged = Object.assign({}, first, second);

        // IF UNION
        let mergedKeys = Object.keys(merged);
        // IF INTERSECTION
        mergedKeys = mergedKeys.filter((key) => {
           return (first[key] && second[key]);
        });

        // since keys are strings of sortKey, just sort
        mergedKeys.sort();

        // TODO: Define empty object
        let emptyObject = {
            calculation: ""
        };

        let missingFromFirst = [];
        let missingFromSecond = [];

        // Now we need to reset the objects to follow the new order with new keys
        this.firstObj = mergedKeys.map((key) => {
            if(first[key]) //if the map contains this key
                return first[key];
            missingFromFirst.push(key);
            return emptyObject; //TODO: this will be a blank object
        });
        this.secondObj = mergedKeys.map((key) => {
            if(second[key]) //if the map contains this key
                return second[key];
            missingFromSecond.push(key);
            return emptyObject; //TODO: this will be a blank object
        });

        console.log('Miss Rate 1', missingFromFirst.length + ':' + Object.keys(first).length);
        console.log('Miss Rate 2', missingFromSecond.length + ':' + Object.keys(second).length);

        this.firstObj.forEach(element => {
            ctr += 1;
            var second = this.secondObj[ctr]
            var calculation = element.calculation;
            if (second == null) {
                return;
            }
            var difference = percentDiff(parseFloat(element.output), parseFloat(second['output']));

            var isDifferent = Math.abs(difference) > Math.abs(this.threshold);
            if (!isDifferent) {
                matchingRows += 1;
            }
            calculation = calculation.replace(/(\r\n|\n|\r)/gm, '<br />')
            if (this.conflictsOnly && !isDifferent) {
                return;
            }
            body += `<tr>`;
            body += `<td valign="top">${element.sortKey}</td>`;
            body += `<td valign="top">${element.carCode}</td>`;
            body += `<td valign="top">${element.ProductCode}</td>`;
            body += `<td valign="top">${element.duration}</td>`;
            body += `<td valign="top">${element.startDate}</td>`;
            if (second != null) {
                body += `<td valign="top" ${(isDifferent ? 'class=remove' : '')}>${second['output']}</td>`;
            } else {
                body += `<td valign="top"></td>`;
            }
            body += `<td valign="top" ${(isDifferent ? 'class=remove' : '')}>${element.output}</td>`;
            body += `<td valign="top">${_.round(difference, 2).toString()}%</td> `;
            body += `<td valign="top">${calculation}</td>`;
            body += `</tr>`;
        });


        summary += `<table>
        <tr>
            <th>Test Summary</th>
        </tr>
        <tr>
            <td>Percentage: </td>
            <td>${_.round((100 * matchingRows) / totalRows, 2)}%</td>
        </tr>
        <tr>
            <td>Rows Matched: </td>
            <td>${matchingRows} / ${totalRows}</td>
        </tr>
        </table><br /><br />`;

        return { body, summary };
    }

    generateFireflyBody() {
        var body = '';
        var summary = '';
        //generate headers
        body += `
        <table><tr>
        <th>Car Code</th>
        <th>Product Code</th>
        <th>Duration</th>
        <th>Season Start</th>
        <th>Comparison Output (MarginFuel)</th>
        <th>Calculated Output</th>
        <th>Difference %</th>
        <th>Calculation</th>
        </tr>`;

        var ctr = -1;
        var matchingRows = 0;
        var totalRows = this.firstObj.length;
        this.firstObj.forEach(element => {
            ctr += 1;
            var second = this.secondObj[ctr];
            var calculation = element.calculation;
            console.log(percentDiff(parseFloat(element.output), parseFloat(second['Computed Price'])));
            var difference = percentDiff(parseFloat(element.output), parseFloat(second['Computed Price']));

            var isDifferent = Math.abs(difference) > Math.abs(this.threshold);
            if (!isDifferent) {
                matchingRows += 1;
            }
            calculation = calculation.replace(/(\r\n|\n|\r)/gm, '<br />')
            if (this.conflictsOnly && !isDifferent) {
                return;
            }
            body += `<tr>`;
            body += `<td valign="top">${element.carCode}</td>`;
            body += `<td valign="top">${element.ProductCode}</td>`;
            body += `<td valign="top">${element.duration}</td>`;
            body += `<td valign="top">${element.seasonStart}</td>`;
            body += `<td valign="top" ${(isDifferent ? 'class=remove' : '')}>${element.output}</td>`;
            if (second != null) {
                body += `<td valign="top" ${(isDifferent ? 'class=remove' : '')}>${second['Computed Price']}</td>`;
            } else {
                body += `<td valign="top"></td>`;
            }
            body += `<td valign="top">${_.round(difference, 2).toString()}%</td> `;
            body += `<td valign="top">${calculation}</td>`;
            body += `</tr>`;
        });


        summary += `<table>
        <tr>
            <th>Test Summary</th>
        </tr>
        <tr>
            <td>Percentage: </td>
            <td>${_.round((100 * matchingRows) / totalRows, 2)}%</td>
        </tr>
        <tr>
            <td>Rows Matched: </td>
            <td>${matchingRows} / ${totalRows}</td>
        </tr>
        </table><br /><br />`;

        return { body, summary };
    }

    generateAceAUBody() {
        var body = '';
        var summary = '';
        this.firstObj = _.sortBy(this.firstObj, sec => {
            var index = _.findIndex(this.secondObj,
                {
                    'Rate_prod': sec['Rate_prod'],
                    'Rate_eff_date': sec['Rate_eff_date'],
                    'Rate_class': sec['Rate_class']
                }
            );
            return index;
        });
        //generate headers
        body += `
        <table><tr>
        <th>Rate_prod</th>
        <th>Rate_eff_date</th>
        <th>Rate_class</th>
        <th>Rate_day (MarginFuel)</th>
        <th>Rate_day (Calculated)</th>
        <th>Difference</th>
        <th>Rate_week (MarginFuel)</th>
        <th>Rate_week (Calculated)</th>
        <th>Difference</th>
        <th>Rate_month (MarginFuel)</th>
        <th>Rate_month (Calculated)</th>
        <th>Difference</th>
        <th>Rate_xday (MarginFuel)</th>
        <th>Rate_xday (Calculated)</th>
        <th>Difference</th>
        <th>Calculation</th>
        </tr>`;

        var ctr = -1;
        var matchingRows = 0;
        var matchingRecords = 0;
        var totalRows = this.firstObj.length;
        var totalRecords = this.firstObj.length * 4;
        this.secondObj.forEach(element => {
            ctr += 1;
            var second = this.firstObj[_.findIndex(this.firstObj,
                {
                    'Rate_prod': element['Rate_prod'],
                    'Rate_eff_date': element['Rate_eff_date'],
                    'Rate_class': element['Rate_class']
                })];
            // var second = this.firstObj[ctr];
            if (second == null) {
                return;
            }
            var calculation = second.Calculation;
            // console.log(percentDiff(parseFloat(element.output), parseFloat(second['Computed Price'])));


            // var isDifferent =
            calculation = calculation.replace(/(\r\n|\n|\r)/gm, '<br />')

            var dayDifferent = this.isDifferent(element.Rate_day, second.Rate_day);
            var weekDifferent = this.isDifferent(element.Rate_week, second.Rate_week);
            var monthDifferent = this.isDifferent(element.Rate_month, second.Rate_month);
            var xDayDifferent = this.isDifferent(element.Rate_xday, second.Rate_xday);

            if (!dayDifferent && !weekDifferent && !monthDifferent && !xDayDifferent) {
                matchingRows += 1;
            }
            if (!dayDifferent) {
                matchingRecords += 1;
            }
            if (!weekDifferent) {
                matchingRecords += 1;
            }
            if (!monthDifferent) {
                matchingRecords += 1;
            }
            if (!xDayDifferent) {
                matchingRecords += 1;
            }

            if (this.conflictsOnly && (!dayDifferent && !weekDifferent && !monthDifferent && !xDayDifferent)) {
                return;
            }

            body += `<tr>`;
            body += `<td valign="top">${element.Rate_prod}</td>`;
            body += `<td valign="top">${element.Rate_eff_date}</td>`;
            body += `<td valign="top">${element.Rate_class}</td>`;
            body += `<td valign="top" ${dayDifferent ? 'class=remove' : ''}>${element.Rate_day}</td>`;
            body += `<td valign="top" ${dayDifferent ? 'class=remove' : ''}>${second.Rate_day}</td>`;
            body += `<td valign="top">${_.round(percentDiff(parseFloat(element.Rate_day), parseFloat(second.Rate_day)), 2).toString()}%</td> `;
            body += `<td valign="top" ${weekDifferent ? 'class=remove' : ''}>${element.Rate_week}</td>`;
            body += `<td valign="top" ${weekDifferent ? 'class=remove' : ''}>${second.Rate_week}</td>`;
            body += `<td valign="top">${_.round(percentDiff(parseFloat(element.Rate_week), parseFloat(second.Rate_week)), 2).toString()}%</td> `;
            body += `<td valign="top" ${monthDifferent ? 'class=remove' : ''}>${element.Rate_month}</td>`;
            body += `<td valign="top" ${monthDifferent ? 'class=remove' : ''}>${second.Rate_month}</td>`;
            body += `<td valign="top">${_.round(percentDiff(parseFloat(element.Rate_month), parseFloat(second.Rate_month)), 2).toString()}%</td> `;
            body += `<td valign="top" ${xDayDifferent ? 'class=remove' : ''}>${element.Rate_xday}</td>`;
            body += `<td valign="top" ${xDayDifferent ? 'class=remove' : ''}>${second.Rate_xday}</td>`;
            body += `<td valign="top">${_.round(percentDiff(parseFloat(element.Rate_xday), parseFloat(second.Rate_xday)), 2).toString()}%</td> `;
            body += `<td valign="top" style="min-width:200px">${calculation}</td>`;
            body += `</tr>`;
        });

        summary += `<table>
        <tr>
            <th>Test Summary</th>
        </tr>
        <tr>
            <td>Percentage: </td>
            <td>${_.round((100 * matchingRecords) / totalRecords, 2)}%</td>
        </tr>
        <tr>
            <td>Rows Matched: </td>
            <td>${matchingRows} / ${totalRows}</td>
        </tr>
        <tr>
            <td>Records Matched: </td>
            <td>${matchingRecords} / ${totalRecords}</td>
        </tr>
        </table><br /><br />`;

        return { body, summary };
    }

    isDifferent(val1, val2) {
        var difference = percentDiff(parseFloat(val1), parseFloat(val2));
        return Math.abs(difference) > Math.abs(this.threshold);
    }

    toString() {
        return this.result;
    }

}

module.exports = CSVDiffHTMLRenderer;
