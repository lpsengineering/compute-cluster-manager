const HtmlRenderer = require('./src/htmlRenderer');
const fs = require('fs');

// var first = fs.readFileSync('./test_computed_output2.csv','utf8').toString();
var first = fs.readFileSync('./HZ Test/output.csv', 'utf8').toString();
// var second = fs.readFileSync('./test_output_compare2.csv', 'utf8').toString();
var second = fs.readFileSync('./HZ Test/hertz6.csv', 'utf8').toString();

var html = new HtmlRenderer(first, second, { source: 'Hertz' }).toString();

fs.writeFileSync('test_html.html', html, 'utf8');
